import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AccountService } from '../_services/account.service';

@Injectable({
  providedIn: 'root'
})
export class ColabGuard implements CanActivate {
  constructor(private accountService: AccountService, private router: Router) { }

  canActivate(): Observable<boolean> {

    return this.accountService.currentUser$.pipe(
      map(user => {
        if (user) {
          if (user.isColaborador) return true;

          this.router.navigate(['dolar'])
        }
        this.router.navigateByUrl('/');
          return false;
      })
    );
  }

}
