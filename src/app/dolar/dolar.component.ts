import { Component, OnInit } from '@angular/core';
import { DolarService } from '../_services/dolar.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgxChartsModule } from '@swimlane/ngx-charts';

@Component({
  selector: 'app-dolar',
  templateUrl: './dolar.component.html',
  styleUrls: ['./dolar.component.css']
})
export class DolarComponent implements OnInit {

  chartPage: boolean = false;
  pageSize = 10;
  page = 1;
  numberOfPages: number = 0;
  items = []


  data = [
    {
      "name": "Germany",
      "series": [
        {
          "name": "2010",
          "value": 7300000
        },
        {
          "name": "2011",
          "value": 8940000
        }
      ]
    },

    {
      "name": "USA",
      "series": [
        {
          "name": "2010",
          "value": 7870000
        },
        {
          "name": "2011",
          "value": 8270000
        }
      ]
    }
  ]

  view: any[] = [700, 300];

  // options
  legend: boolean = true;
  showLabels: boolean = true;
  animations: boolean = true;
  xAxis: boolean = true;
  yAxis: boolean = true;
  showYAxisLabel: boolean = true;
  showXAxisLabel: boolean = true;
  xAxisLabel: string = 'Year';
  yAxisLabel: string = 'Population';
  timeline: boolean = true;

  colorScheme = {
    domain: ['#5AA454', '#E44D25', '#CFC0BB', '#7aa3e5', '#a8385d', '#aae3f5']
  };


  onSelect(data): void {
    console.log('Item clicked', JSON.parse(JSON.stringify(data)));
  }

  onActivate(data): void {
    console.log('Activate', JSON.parse(JSON.stringify(data)));
  }

  onDeactivate(data): void {
    console.log('Deactivate', JSON.parse(JSON.stringify(data)));
  }

  constructor(private dolarService: DolarService) {
    Object.assign(this,  this.data );
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.getCotacoes(this.page);
    }, 1);
  }

  getCotacoes(page: number) {

    this.dolarService.getAllItems(page, this.pageSize, null).subscribe(clientes => {
      this.items = clientes.data;
      this.page = clientes.pageNumber;
      this.pageSize = clientes.pageSize;
      this.numberOfPages = clientes.totalPages;
    });
  }

  changeTab() {
    this.chartPage = !this.chartPage;
  }

}
