import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { NavigationExtras, Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

  constructor(private router : Router , private toastr : ToastrService ) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return next.handle(request).pipe(
      catchError( error => {
        if(error) {
          switch (error.status){
            case 400 :
              this.toastr.error("Erro!");
              break;
            case 401:
              this.toastr.error("Acesso negado.", "Erro!");
              break;
            case 404:
              this.router.navigateByUrl("/not-found");
              this.toastr.error("Não encontrado.", "Erro!");
              break;
            case 500:
              const navigatioExtras : NavigationExtras = {state:{error: error.error}};
              this.router.navigateByUrl("/server-error", navigatioExtras);
              break;
            default:
              this.toastr.error("Algo inesperado ocorreu...", "Erro!");
              console.log(error);
              break
          }
        } return throwError(error);
      }

      )
    );
  }
}
