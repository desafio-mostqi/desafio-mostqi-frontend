import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../_models/user';
import { ClienteService } from '../_services/cliente.service';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css']
})
export class ClientesComponent implements OnInit {

  constructor(private clienteService: ClienteService, private router: Router) { }

  clientes = [];
  page = 1;
  pageSize = 7;
  numberOfPages: number = 0;
  filter = ""

  ngOnInit(): void {
    setTimeout(() => {
      this.getClientes(this.page);
    }, 1);
  }

  getClientes(page: number) {
    
    this.clienteService.getAllClientes(page, this.pageSize,this.filter).subscribe(clientes => {
      this.clientes = clientes.data;
      this.page = clientes.pageNumber;
      this.pageSize = clientes.pageSize;
      this.numberOfPages = clientes.totalPages;
    });
  }

  goToNewCliente(id: number) {
    this.router.navigate(['cliente-details'], { queryParams: { id } });
  }
  
}
