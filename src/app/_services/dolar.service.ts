import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Cotacao } from '../_models/cotacao';
import { PaginatedData } from '../_models/paginated-data';

@Injectable({
  providedIn: 'root'
})
export class DolarService {

  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  }

  baseUrl: string =  environment.apiUrl + "/cotacao"

  getAllItems(pageNumber :number,size: number, data : Date) {
    let str ;
    if(data)  str = `?pageNumber=${pageNumber}&pageSize=${size}&filter=${data}`;
    else  str = `?pageNumber=${pageNumber}&pageSize=${size}&filter=`;
    return this.http.get<PaginatedData<Cotacao>>(this.baseUrl + str, this.httpOptions)
      .pipe(
        retry(0),
        catchError(this.handleError)
      )
  }

  handleError(error: HttpErrorResponse) {
    return throwError(error);
  };
}
