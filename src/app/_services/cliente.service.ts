import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError, map, retry } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { PaginatedData, } from '../_models/paginated-data';
import { User } from '../_models/user';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {

  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  }

  baseUrl: string = environment.apiUrl + "/cliente"

  getAllClientes(pageNumber: number, size: number, filter:string) {
    return this.http.get<PaginatedData<User>>(this.baseUrl + `?pageNumber=${pageNumber}&pageSize=${size}&cliente=${filter}`, this.httpOptions)
      .pipe(
        retry(0),
        catchError(this.handleError)
      )
  }
  getClienteById(id: number) {
    return this.http.get<User>(this.baseUrl + `/${id}`, this.httpOptions)
      .pipe(
        retry(0),
        catchError(this.handleError)
      )
  }
  updateCliente(client: User) {
    return this.http.put<User>(this.baseUrl, JSON.stringify(client), this.httpOptions)
      .pipe(
        retry(0),
        catchError(this.handleError)
      )
  }



  handleError(error: HttpErrorResponse) {
    return throwError(error);
  };
}
