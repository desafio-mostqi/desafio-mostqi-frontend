import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { ReplaySubject, throwError } from 'rxjs';
import { catchError, map, retry } from 'rxjs/operators';
import { User } from '../_models/user';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  baseUrl: string = environment.apiUrl + "/account";

  private currentuserSource = new ReplaySubject<User>(1);
  currentUser$ = this.currentuserSource.asObservable();

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  }

  constructor(private http: HttpClient) { }

  login(model) {
    return this.http.post(this.baseUrl + "/login", model).pipe(
      map((response: User) => {
        const user = response;
        if (user) {
          localStorage.setItem('user', JSON.stringify(user));
          this.currentuserSource.next(user);
        }
      })
    )
  }


  logout() {
    localStorage.removeItem('user');
    this.currentuserSource.next(null);
  }

  mostQiAuth() {

  }

  register(model) {
    return this.http.post(this.baseUrl + "/register-colaborador", model).pipe(
      map((response: User) => {
        const user = response;
        user.currentUser = response.id;
        if (user) {
          localStorage.setItem('user', JSON.stringify(user));
          this.currentuserSource.next(user);
        }
      })
    )
  }

  registerCliente(model: User) {
    return this.http.post(this.baseUrl + "/register-cliente", JSON.stringify(model), this.httpOptions).pipe(
      retry(0),
      catchError(this.handleError)
    )
  }

  setCurrentUser(user: User) {
    this.currentuserSource.next(user);
  }

  handleError(error: HttpErrorResponse) {
    return throwError(error);
  };
}
