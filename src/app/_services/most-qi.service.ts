import { HttpClient, HttpHeaders } from '@angular/common/http';
import { utf8Encode } from '@angular/compiler/src/util';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { ExtractedData } from '../_models/extracted-image';

@Injectable({
  providedIn: 'root'
})
export class MostQiService {


  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  }

  baseUrl: string = environment.apiUrl + "/mostqi"

  contentExtraction(fileBase64: string) {
    return this.http.post(this.baseUrl + "/process-image", JSON.stringify({
      fileBase64,
      "fileUrl": "",
      "returnImage": false,
      "type": "",
      "tag": ""
    }), this.httpOptions).pipe(
      map((response: ExtractedData) => {
        return response;
      })
    )
  }
  faceFeatureExtract(fileBase64: string) {
    return this.http.post(this.baseUrl + "/face-extract-features", JSON.stringify({
      fileBase64,
      "fileUrl": "",
      "returnImage": false,
      "type": "",
      "tag": ""
    }), this.httpOptions).pipe(
      map((response: ExtractedData) => {
        return response;
      })
    )
  }

}
