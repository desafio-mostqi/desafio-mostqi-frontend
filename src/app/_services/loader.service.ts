import { Injectable } from '@angular/core';
import { NgxSpinnerService } from 'ngx-bootstrap-spinner';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {



  public isLoading: BehaviorSubject<boolean>;

  constructor() {
    this.isLoading = new BehaviorSubject<boolean>(false);

  }
}
