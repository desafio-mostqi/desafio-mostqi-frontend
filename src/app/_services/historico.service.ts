import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { utf8Encode } from '@angular/compiler/src/util';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { HistoricoItem } from '../_models/historico-item';
import { PaginatedData } from '../_models/paginated-data';

@Injectable({
  providedIn: 'root'
})
export class HistoricoService {

  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  }

  baseUrl: string =  environment.apiUrl + "/historico"

  getAllItems(pageNumber :number,size: number, colab : string, client: string) {
    
    return this.http.get<PaginatedData<HistoricoItem>>(this.baseUrl + `?pageNumber=${pageNumber}&pageSize=${size}&colab=${colab}&client=${client}`, this.httpOptions)
      .pipe(
        retry(0),
        catchError(this.handleError)
      )
  }

  handleError(error: HttpErrorResponse) {
    return throwError(error);
  };
}
