import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-confirm-modal',
  templateUrl: './confirm-modal.component.html',
  styleUrls: ['./confirm-modal.component.css']
})
export class ConfirmModalComponent implements OnInit {

  public onClose: Subject<boolean>;

  public data: string;

  checkboxData = []

  constructor(private _bsModalRef: BsModalRef, public options: ModalOptions) { }

  public ngOnInit(): void {
    this.onClose = new Subject();
    console.log(console.log(this.options))
    this.data = this.options['content']
    console.log(this.data)



    Object.keys(this.data).forEach((item) => {
      if (this.data[item])
        this.checkboxData.push({name : item.charAt(0).toUpperCase() + item.slice(1), value : this.data[item]})
    })
  }



  public onConfirm(): void {
    this.onClose.next(true);
    this._bsModalRef.hide();
  }

  public onCancel(): void {
    this.onClose.next(false);
    this._bsModalRef.hide();
  }
}
