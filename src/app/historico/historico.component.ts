import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HistoricoService } from '../_services/historico.service';

@Component({
  selector: 'app-historico',
  templateUrl: './historico.component.html',
  styleUrls: ['./historico.component.css']
})
export class HistoricoComponent implements OnInit {

  constructor(private historicoService: HistoricoService, private router: Router) { }

  items = [];
  page = 1;
  pageSize = 10;
  numberOfPages: number = 0;
  filter = ""

  type = "Colaborador";

  ngOnInit(): void {
    setTimeout(() => {
      this.getHistorico(this.page);
    }, 1);

  }
  setType(value: string) {
    this.type = value;
  }

  getHistorico(page: number) {

    if (this.type == "Colaborador")
      this.historicoService.getAllItems(page, this.pageSize, this.filter, "").subscribe(clientes => {
        this.items = clientes.data;
        this.page = clientes.pageNumber;
        this.pageSize = clientes.pageSize;
        this.numberOfPages = clientes.totalPages;
        
      });
    else {
      this.historicoService.getAllItems(page, this.pageSize, "", this.filter).subscribe(clientes => {
        this.items = clientes.data;
        this.page = clientes.pageNumber;
        this.pageSize = clientes.pageSize;
        this.numberOfPages = clientes.totalPages;
      });
    }
  }

  goToNewCliente(id: number) {
    this.router.navigate(['cliente-details'], { queryParams: { id } });
  }
}
