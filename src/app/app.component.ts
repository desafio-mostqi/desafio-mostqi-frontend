import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { User } from './_models/user';
import { AccountService } from './_services/account.service';
import { LoaderService } from './_services/loader.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'frontend';

  url = 'https://localhost:5001';

  users: any;

  constructor(public accountService: AccountService, public loaderService: LoaderService, private router : Router, private toastr : ToastrService) { }

  ngOnInit(): void {
    this.setCurrentUser();

  }

  setCurrentUser(){
    const user : User = JSON.parse(localStorage.getItem('user'));
    this.accountService.setCurrentUser(user);
    if(user){
      if(user.isColaborador) this.router.navigate(['clientes/'])
      else this.router.navigate(['dolar/'])
    }
  }
}
