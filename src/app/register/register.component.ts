import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AccountService } from '../_services/account.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  @Output() cancelRegister = new EventEmitter();

  model: any = {}

  validationForm: FormGroup;

  constructor(private accountService: AccountService,
    private router: Router,
    private toastrService: ToastrService,
    private formBuilder: FormBuilder,) { }

  ngOnInit(): void {
    this.buildForm()
  }

  buildForm() {
    this.validationForm = this.formBuilder.group({
      userName: this.formBuilder.control(null, [Validators.required, Validators.minLength(6), Validators.maxLength(100)]),
      password: this.formBuilder.control(null, [Validators.required, Validators.minLength(6), Validators.maxLength(100)]),
      password2: this.formBuilder.control(null, [Validators.required, Validators.minLength(6), Validators.maxLength(100)]),

    }, { validators: this.checkPasswords });

  }

  checkPasswords: ValidatorFn = (group: AbstractControl): ValidationErrors | null => {
    let pass = group.get('password').value;
    let confirmPass = group.get('password2').value
    return pass === confirmPass ? null : { notSame: true }
  }


  register() {
    this.accountService.register(
      {
        username: this.validationForm.get("userName").value,
        password: this.validationForm.get("password").value
      }).subscribe(
        response => {
          this.cancel();
          this.router.navigateByUrl("/clientes");
          this.toastrService.success("Colaborador criado.", "Sucesso!")
        }, error => {
          //this.toastr.error("Impossible to register","Error");
        });
  }

  cancel() {
    this.cancelRegister.emit(false);
  }

}
