import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AccountService } from '../_services/account.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @Output() cancelLogin = new EventEmitter();

  model: any = {}

  validationForm: FormGroup;

  constructor(private accountService: AccountService,
    private router: Router,
    private toastrService: ToastrService,
    private formBuilder: FormBuilder,) { }

  ngOnInit(): void {
    this.buildForm()
  }

  ngAfterViewInit() {
    this.buildForm()
  }

  buildForm() {
    this.validationForm = this.formBuilder.group({
      userName : this.formBuilder.control(null, [Validators.required, Validators.minLength(6), Validators.maxLength(100)]),
      password: this.formBuilder.control(null, [Validators.required, Validators.minLength(6), Validators.maxLength(100)]),


    });

  }

  login() {
    this.accountService.login({
      username: this.validationForm.get("userName").value,
      password: this.validationForm.get("password").value
    }).subscribe(
      response => {
        this.accountService.currentUser$.subscribe(user => {
          console.log(user)
          this.toastrService.success(`Bem Vindo(a) ${user.username}!`)
          if (user.isColaborador) this.router.navigate(['clientes/']);
          else this.router.navigate(['dolar'])
        })
        this.validationForm.reset()
      },
    )
  }


  cancel() {
    this.cancelLogin.emit(true);
  }


}
