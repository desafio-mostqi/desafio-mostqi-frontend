import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClienteDatailsComponent } from './cliente-datails/cliente-datails.component';
import { ClientesComponent } from './clientes/clientes.component';
import { DolarComponent } from './dolar/dolar.component';
import { HistoricoComponent } from './historico/historico.component';
import { HomeComponent } from './home/home.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { AuthGuard } from './_guards/auth.guard';
import { ColabGuard } from './_guards/colab.guard';

const routes: Routes = [
  { path: '', component: HomeComponent },
  {
    path: '',
    runGuardsAndResolvers: 'always',
    canActivate: [AuthGuard],
    children: [
      { path: 'clientes', component: ClientesComponent, canActivate: [AuthGuard, ColabGuard] },
      { path: 'cliente-details', component: ClienteDatailsComponent, canActivate: [AuthGuard, ColabGuard] },
      { path: 'historico', component: HistoricoComponent , canActivate: [AuthGuard, ColabGuard]},
      { path: 'dolar', component: DolarComponent , canActivate: [AuthGuard]},

      // { path: 'lists', component: ListsComponent },
    ]
  },
  // { path: 'not-found', component: NotFoundComponent },
  { path: '**', component: NotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
