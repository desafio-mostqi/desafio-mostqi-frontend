import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { ConfirmModalComponent } from '../common/confirm-modal/confirm-modal.component';
import { ExtractedData } from '../_models/extracted-image';
import { User } from '../_models/user';
import { AccountService } from '../_services/account.service';
import { ClienteService } from '../_services/cliente.service';
import { MostQiService } from '../_services/most-qi.service';

@Component({
  selector: 'app-cliente-datails',
  templateUrl: './cliente-datails.component.html',
  styleUrls: ['./cliente-datails.component.css']
})
export class ClienteDatailsComponent implements OnInit {

  constructor(private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private _sanitizer: DomSanitizer,
    private accountService: AccountService,
    private mostQiService: MostQiService,
    private toastrService: ToastrService,
    private router: Router,
    private modalService: BsModalService,
    private clienteService: ClienteService) { }

  validationForm: FormGroup;
  isNewClient = true;
  idClient: number;
  title: string = "";
  cliente: User = new User();
  public modalRef: BsModalRef;

  imageError: string;
  image64: string = "";
  isImageSaved: boolean;
  cardImageBase64: string;
  extractedData: ExtractedData;

  imagePath

  ngOnInit(): void {
    this.route.queryParams.subscribe(param => {
      this.idClient = param['id'];

    });
    if (this.idClient != 0 && this.idClient) {
      this.isNewClient = false;
      setTimeout(() => {
        this.getCliente(this.idClient);
      }, 1);

    } else {
      this.isNewClient = true;
    }
    this.buildForm();
  }


  getCliente(id: number) {
    this.clienteService.getClienteById(id).subscribe(c => {
      this.cliente = c;
      this.validationForm.controls["userName"].setValue(c.username);
      this.validationForm.controls["nome"].setValue(c.name);
      this.validationForm.controls["rg"].setValue(c.rg);
      this.validationForm.controls["nascimento"].setValue(c.dataNascimento);
      if (c.imageBase64)
        this.base64ToImage(c.imageBase64);
    })
  }

  buildForm() {
    if (this.isNewClient) {
      this.validationForm = this.formBuilder.group({
        userName: this.formBuilder.control(null, [Validators.required, Validators.minLength(6), Validators.maxLength(100)]),
        password: this.formBuilder.control(null, [Validators.required, Validators.minLength(6), Validators.maxLength(100)]),
        password2: this.formBuilder.control(null, [Validators.required, Validators.minLength(6), Validators.maxLength(100)]),
        nome: this.formBuilder.control(null, [Validators.required, Validators.minLength(6), Validators.maxLength(100)]),
        rg: this.formBuilder.control(null, [Validators.required, Validators.minLength(6), Validators.maxLength(100)]),
        nascimento: this.formBuilder.control(null, [Validators.required, Validators.minLength(6), Validators.maxLength(22)]),
      }, { validators: this.checkPasswords });
    }
    else {
      this.validationForm = this.formBuilder.group({
        userName: this.formBuilder.control(null, [Validators.required, Validators.minLength(6), Validators.maxLength(100)]),
        nome: this.formBuilder.control(null, [Validators.required, Validators.minLength(6), Validators.maxLength(100)]),
        rg: this.formBuilder.control(null, [Validators.required, Validators.minLength(6), Validators.maxLength(100)]),
        nascimento: this.formBuilder.control(null, [Validators.required, Validators.minLength(6), Validators.maxLength(22)]),
      });
    }
  }

  fileChangeEvent(fileInput: any) {
    this.imageError = null;
    if (fileInput.target.files && fileInput.target.files[0]) {
      // Size Filter Bytes
      const max_size = 20971520;
      const allowed_types = ['image/jpeg'];
      const max_height = 15200;
      const max_width = 25600;

      if (fileInput.target.files[0].size > max_size) {
        this.imageError =
          'Maximum size allowed is ' + max_size / 1000 + 'Mb';

        return false;
      }
      if (!allowed_types.includes(fileInput.target.files[0].type)) {
        this.imageError = 'Only Images are allowed ( JPG  )';
        return false;
      }
      const reader = new FileReader();
      reader.onload = (e: any) => {
        const image = new Image();
        image.src = e.target.result;
        image.onload = rs => {
          const img_height = rs.currentTarget['height'];
          const img_width = rs.currentTarget['width'];

          if (img_height > max_height && img_width > max_width) {
            this.imageError =
              'Maximum dimentions allowed ' +
              max_height +
              '*' +
              max_width +
              'px';
            return false;
          } else {
            const imgBase64Path = e.target.result;
            this.cardImageBase64 = imgBase64Path;
            this.isImageSaved = true;
            // this.previewImagePath = imgBase64Path;

            // Call for the service
            this.mostQiService.contentExtraction(this.cardImageBase64.substring(23)).subscribe(data => {
              this.extractedData = data;
              //this.fillFields(this.extractedData);
              this.mostQiService.faceFeatureExtract(this.cardImageBase64.substring(23)).subscribe(facialData => {
                //console.log(facialData.result[0].image)
                if (facialData.result.length > 0) {
                  this.image64 = facialData.result[0].image;
                  if (facialData.result[0].image.length > 0)
                    this.base64ToImage(facialData.result[0].image);
                }
                this.openConfirmDialog(data);
              })

              // console.log(data.result[0].image)
              // if (data.result.length > 0)
              //   this.base64ToImage(data.result[0].image);
            });

            //console.log(this.cardImageBase64);

          }
        };
      };
      reader.readAsDataURL(fileInput.target.files[0]);
    }
  }
  saveClient() {
    this.cliente.username = this.validationForm.get("userName").value
    this.cliente.rg = this.validationForm.get("rg").value
    this.cliente.name = this.validationForm.get("nome").value
    this.cliente.dataNascimento = this.validationForm.get("nascimento").value
    this.cliente.imageBase64 = this.image64;
    this.accountService.currentUser$.subscribe(user => {
      this.cliente.currentUser = user.id;
      
      if (this.isNewClient) {
        this.cliente.password = this.validationForm.get("password").value
        this.accountService.registerCliente(this.cliente).subscribe(data => {
          this.toastrService.success("O cliente foi adicionado.", "Sucesso!")
          this.backToHome();
        })
      } else {
        this.clienteService.updateCliente(this.cliente).subscribe(data => {
          this.toastrService.success("As alterações foram salvas.", "Sucesso!")
          this.backToHome();
        })
      }


    })
  }

  base64ToImage(base64string: string) {
    this.imagePath = this._sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,'
      + base64string);
  }

  checkPasswords: ValidatorFn = (group: AbstractControl): ValidationErrors | null => {
    let pass = group.get('password').value;
    let confirmPass = group.get('password2').value
    return pass === confirmPass ? null : { notSame: true }
  }

  fillFields(data) {
    if (data['nascimento']) this.validationForm.controls["nascimento"].setValue(data['nascimento']);
    if (data['rg']) this.validationForm.controls["rg"].setValue(data['rg']);
    if (data['nome']) this.validationForm.controls["nome"].setValue(data['nome']);
  }

  openConfirmDialog(data: ExtractedData) {
    let content = { "nascimento": null, "rg": null, "nome": null }

    if (data.result.length > 0) {
      data.result[0].fields.forEach(field => {
        if (field.name == "data_nascimento") {
          content.nascimento = field.value;
        }
        else if (field.name == "rg") {
          content.rg = field.value;
        }
        else if (field.name == "nome") {
          content.nome = field.value
        }
      })
    }

    let modalConfig = { animated: true, keyboard: true, backdrop: true, ignoreBackdropClick: true };
    this.modalRef = this.modalService.show(ConfirmModalComponent, Object.assign({ "oi": "d" }, modalConfig, {
      content
    }));

    this.modalRef.content.onClose.subscribe(result => {
      if (result) {
        this.fillFields(content)
      } else {

      }
    })
  }

  removeImage() {
    this.cardImageBase64 = null;
    this.isImageSaved = false;
  }

  backToHome() {
    this.router.navigate(["clientes"])
  }

}
