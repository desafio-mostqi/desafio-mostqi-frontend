import { User } from "./user";


export class PaginatedData <T> {
    data: T[];
    succeeded: boolean;
    pageNumber: number;
    pageSize: number;
    totalPages: number;
    totalRecords: number;
    
}

