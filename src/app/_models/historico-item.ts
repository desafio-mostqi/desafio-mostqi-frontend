import { User } from "./user"

export class HistoricoItem {
    id: number
    data: Date
    acao: string
    cloaboradorName: string
    cloaboradorId: number
    clienteName : string
    clienteId : number
}