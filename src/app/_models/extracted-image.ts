export interface ExtractedData {
    result: Result[];
    requestId: string;
    elapsedMilliseconds: number;
    status: Status;
}


export interface Vertex {
    name: string;
    x: number;
    y: number;
}
export interface Result {
    fields?: Field[];
    image?: any;
    score: number;
    type?: any;
    stdType?: any;
    pageNumber: number;
    tags?: any;
    tables?: any;
    feature: string;
    vertices: Vertex[];
    area: number;
    processType?: any;
}
export interface Status {
    message: string;
    code: number;
    errors?: any;
}
export interface Field {
    name: string;
    stdName?: any;
    value: string;
    score: number;
}



export interface RootObject {
    result: Result[];
    requestId: string;
    elapsedMilliseconds: number;
    status: Status;
}