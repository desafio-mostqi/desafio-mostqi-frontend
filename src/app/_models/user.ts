export class User{
    id:number
    username : string;
    name : string;
    rg: string;
    dataNascimento: string
    token : string;
    isColaborador: boolean;
    mostQiToken:string;
    currentUser: number;
    password: string
    imageBase64: string;
}